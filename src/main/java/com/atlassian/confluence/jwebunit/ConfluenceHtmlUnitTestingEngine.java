package com.atlassian.confluence.jwebunit;

import com.gargoylesoftware.htmlunit.Cache;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlButtonInput;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlResetInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import net.sourceforge.jwebunit.exception.UnableToSetFormException;
import net.sourceforge.jwebunit.htmlunit.HtmlUnitTestingEngineImpl;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.normalizeSpace;

/**
 * This subclasses the standard {@link net.sourceforge.jwebunit.htmlunit.HtmlUnitTestingEngineImpl} which configures,
 * overrides and hacks it to provide what we need for Confluence.
 */
public class ConfluenceHtmlUnitTestingEngine extends HtmlUnitTestingEngineImpl {
    /**
     * Maximum number of connections per host.
     */
    private static final int MAX_CONNECTIONS = 20;

    private static HtmlUnitConfig GLOBAL_CONFIG;

    public static void setGlobalConfig(HtmlUnitConfig timeoutConfig) {
        GLOBAL_CONFIG = timeoutConfig;
    }

    {
        if (GLOBAL_CONFIG == null) {
            throw new IllegalStateException("TIMEOUT_CONFIG has not yet been set, cannot instantiate");
        }
        setThrowExceptionOnScriptError(false);
    }

    private static final Cache WEBCLIENT_REQUEST_CACHE;

    static {
        WEBCLIENT_REQUEST_CACHE = new Cache() {
            @Override
            protected boolean isCacheable(WebRequest request, WebResponse response) {
                final boolean isGetRequestForStaticContent = super.isCacheable(request, response);
                final boolean isRequestForNoopJsp = response.getWebRequest().getUrl().toString().contains("noop.jsp");
                final boolean hasNoRequestParameters = request.getRequestParameters().isEmpty();
                final boolean requestHasNoQueryString = !response.getWebRequest().getUrl().toString().contains("?");

                return isGetRequestForStaticContent
                        || (isRequestForNoopJsp && hasNoRequestParameters && requestHasNoQueryString);
            }

        };
        WEBCLIENT_REQUEST_CACHE.setMaxSize(200);
    }

    public static void clearCache() {
        WEBCLIENT_REQUEST_CACHE.clear();
    }

    @Override
    protected WebClient createWebClient() {
        WebClient webClient = new WebClient(GLOBAL_CONFIG.getBrowserVersion());
        webClient.setWebConnection(new ConfluenceWebConnection(webClient, MAX_CONNECTIONS, GLOBAL_CONFIG));
        webClient.setCache(WEBCLIENT_REQUEST_CACHE);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setPrintContentOnFailingStatusCode(false);

        setTimeout(GLOBAL_CONFIG.getHttpRequestTimeoutMillis());

        return webClient;
    }

    /**
     * Overridden to strip extraneous whitespace from the page text to easier string matching.
     */
    @Override
    public String getPageText() {
        Page page = getCurrentWindow().getEnclosedPage();
        if (page instanceof HtmlPage) {
            return normalizeSpace(((HtmlPage) page).asText());
        } else {
            return super.getPageText();
        }
    }

    /**
     * Overridden to supply a better error message.
     */
    @Override
    public void setWorkingForm(String nameOrId, int index) {
        try {
            super.setWorkingForm(nameOrId, index);
        } catch (UnableToSetFormException e) {
            throw new UnableToSetFormException("Form with [nameOrId='" + nameOrId + "'] and [index=" + index + "] not found on page");
        }
    }

    /**
     * This is a copy of the super class method, but with added handling for the "value" attribute of &lt;button&gt; tags.
     * Why the original doesn't do this already is a mystery.
     */
    @Override
    public HtmlElement getButtonWithText(String buttonValueText) {
        requireNonNull(buttonValueText, "Cannot search for button with null text");

        HtmlElement documentElement = ((HtmlPage) getCurrentWindow().getEnclosedPage()).getDocumentElement();
        List<? extends HtmlElement> l = Stream.of("button", "input")
                .map(documentElement::getElementsByTagName)
                .flatMap(Collection::stream)
                .collect(toList());
        for (HtmlElement e : l) {
            if (e instanceof HtmlButton) {
                // we cannot use asText(), as this returns an empty string if the
                // button is not currently displayed, resulting in different
                // behaviour as the <input> Buttons
                if (buttonValueText.equals(((HtmlButton) e).getTextContent())) {
                    return e;
                }
            }
            if (e instanceof HtmlButtonInput ||
                    e instanceof HtmlSubmitInput ||
                    e instanceof HtmlResetInput ||
                    e instanceof HtmlButton) {
                if (buttonValueText.equals(e.getAttribute("value"))) {
                    return e;
                }
            }
        }
        return null;
    }

    public int getTimeout() {
        return getWebClient().getOptions().getTimeout();
    }

    /**
     * Overridden to allow us to dynamically update the timeouts in HtmlUnit, rather than just on initial setup.
     */
    @Override
    public void setTimeout(int timeout) {
        final WebClient webClient = getWebClient();
        if (webClient != null) {
            // if a client as already been created, then set the timeout on that directly
            // do not call the super method, since that will barf if the client is already initialised
            webClient.getOptions().setTimeout(timeout);
        } else {
            // no client initialised yet, so pass the timeout to the superclass, where it'll be used to create new clients
            super.setTimeout(timeout);
        }
    }

    // The methods below here are reflection hacks to get access to private fields and methods of the superclass
    // it may be better to copy the superclass and modify it to make those members protected.

    protected HtmlForm _form() {
        return reflectionHelper().getPrivateFieldValue("form");
    }

    protected void _form(HtmlForm newForm) {
        reflectionHelper().setPrivateFieldValue("form", newForm);
    }

    private ReflectionHelper<HtmlForm> reflectionHelper() {
        return new ReflectionHelper<>(this, HtmlForm.class, HtmlUnitTestingEngineImpl.class);
    }
}
